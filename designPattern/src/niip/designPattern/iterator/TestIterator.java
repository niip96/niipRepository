package niip.designPattern.iterator;

import niip.designPattern.iterator.itface.Iterator;
import niip.designPattern.iterator.pojo.Book;
import niip.designPattern.iterator.pojo.BookShelf;

/**
 * Created By 86188
 * on 2020/7/5
 * 迭代器模式
 */
public class TestIterator {

    public static void main(String[] args) {
        BookShelf bookShelf = new BookShelf();
        bookShelf.appendBook(new Book("图解设计模式1"));
        bookShelf.appendBook(new Book("Springboot2"));
        bookShelf.appendBook(new Book("Vue3"));
        bookShelf.appendBook(new Book("算法设计与思想4"));
        Iterator<Book> it = bookShelf.iterator();
        while (it.hasNext()) {
            System.out.println(it.next().getName());
        }
    }
}
