package niip.designPattern.iterator.pojo;

import niip.designPattern.iterator.itface.Iterator;

/**
 * Created By NIIP on 2020/7/5
 * Affect: 书架迭代器
 */
public class BookShelfIterator implements Iterator<Book> {

    private BookShelf bookShelf;
    private int cursor;

    public BookShelfIterator(BookShelf bookShelf) {
        this.bookShelf = bookShelf;
        this.cursor = 0;
    }

    @Override
    public boolean hasNext() {
        return cursor < bookShelf.getLength();
    }

    @Override
    public Book next() {
        return bookShelf.getBookAt(cursor++);
    }

}
