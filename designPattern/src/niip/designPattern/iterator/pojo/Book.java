package niip.designPattern.iterator.pojo;

/**
 * Created By NIIP on 2020/7/5
 * Affect:书
 */
public class Book {

    private String name;

    public Book(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
