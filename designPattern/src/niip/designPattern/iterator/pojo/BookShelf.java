package niip.designPattern.iterator.pojo;

import niip.designPattern.iterator.itface.Aggregate;
import niip.designPattern.iterator.itface.Iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By NIIP on 2020/7/5
 * Affect: 书架
 */
public class BookShelf implements Aggregate<Book> {
    //    private Book[] books;
    private List<Book> books;
    private int last = 0;


    //    public BookShelf(int size) {
//        this.books = new Book[size];
//    }
    public BookShelf() {
        this.books = new ArrayList<>();
    }

    public Book getBookAt(int index) {
        if (books.size() == 0) {
            return null;
        }
        if (index < 0) {
            index = 0;
        } else if (index >= books.size()) {
            index = books.size() - 1;
        }
        return books.get(index);
    }

    public void appendBook(Book book) {
        this.books.add(book);
        last++;
    }

    public int getLength() {
        return this.last;
    }

    @Override
    public Iterator<Book> iterator() {
        return new BookShelfIterator(this);
    }
}
