package niip.designPattern.iterator.itface;

/**
 * Created By NIIP on 2020/7/5
 * Affect: 集合接口?
 */
public interface Aggregate<T> {
    /**
     * Created By NIIP on 2020/7/5
     * Affect: 用于生产一个遍历集合的迭代器
     */
    Iterator<T> iterator();
}
