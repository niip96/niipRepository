package niip.designPattern.iterator.itface;

/**
 * Created By NIIP on 2020/7/5
 * Affect: 迭代器接口
 */
public interface Iterator<T> {
    /**
     * 如果下个元素存在则返回true
     *
     * @return
     */
    boolean hasNext();

    /**
     * 当前游标后移,并返回当前元素
     *
     * @return
     */
    T next();
}
