package niip.designPattern.strategy;

import niip.designPattern.strategy.pojo.Cat;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created By 86188
 * on 2020/7/26
 * 策略模式：一个事情，多种不同的方式去实现
 */
public class TestStrategy {

    public static void main(String[] args) {

        Cat[] cats = {new Cat(3, 2), new Cat(1, 1), new Cat(2, 3)};
        // Comparator
        Sorter<Cat> sorter = new Sorter<>();
        sorter.sort(cats, new CatAgeStrategy());
        System.out.println("年龄策略排序：" + Arrays.toString(cats));
        sorter.sort(cats, new CatWeightStrategy());
        System.out.println("重量策略排序：" + Arrays.toString(cats));
    }
}
