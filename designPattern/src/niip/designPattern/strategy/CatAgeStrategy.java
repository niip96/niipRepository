package niip.designPattern.strategy;

import niip.designPattern.strategy.itface.Comparator;
import niip.designPattern.strategy.pojo.Cat;

/**
 * Created By 86188
 * on 2020/7/26
 * 年龄策略类
 */
public class CatAgeStrategy implements Comparator<Cat> {
    /* 年龄大于的返回true */
    @Override
    public boolean compare(Cat t1, Cat t2) {
        return t1.getAge() > t2.getAge();
    }
}
