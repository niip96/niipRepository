package niip.designPattern.strategy;

import niip.designPattern.strategy.itface.Comparator;
import niip.designPattern.strategy.pojo.Cat;

/**
 * Created By 86188
 * on 2020/7/26
 * 体重策略类
 */
public class CatWeightStrategy implements Comparator<Cat> {
    /* 体重小于的返回true */
    @Override
    public boolean compare(Cat t1, Cat t2) {
        return t1.getWeight() < t2.getWeight();
    }
}
