package niip.designPattern.strategy;

import niip.designPattern.strategy.itface.Comparator;

/**
 * Created By 86188
 * on 2020/7/26
 */
public class Sorter<T> {

    /* 选择排序 */
    public void sort(T[] arr, Comparator<T> comparator) {
        for (int i = 0; i < arr.length; i++) {
            int cursor = i;
            for (int j = i + 1; j < arr.length; j++) {
                cursor = comparator.compare(arr[cursor], arr[j]) ? j : cursor;//满足排序规则的返回当前元素下标，否则返回原下标
            }
            if (i != cursor)
                swap(arr, i, cursor);
        }
    }

    /* 交换数组中的起始元素和目标元素 */
    void swap(T[] arr, int origin, int cursor) {
        T t = arr[origin];
        arr[origin] = arr[cursor];
        arr[cursor] = t;
    }
}
