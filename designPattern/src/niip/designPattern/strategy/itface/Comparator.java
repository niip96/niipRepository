package niip.designPattern.strategy.itface;

/**
 * Created By 86188
 * on 2020/7/26
 */
public interface Comparator<T> {
    boolean compare(T t1, T t2);
}
