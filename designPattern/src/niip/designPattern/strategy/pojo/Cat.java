package niip.designPattern.strategy.pojo;

import niip.designPattern.strategy.itface.Comparator;

/**
 * Created By 86188
 * on 2020/7/26
 */
public class Cat {

    private int age, weight;

    public Cat(int age, int weight) {
        this.age = age;
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "age=" + age +
                ", weight=" + weight +
                '}';
    }
}
