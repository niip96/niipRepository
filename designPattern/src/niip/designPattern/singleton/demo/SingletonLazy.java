package niip.designPattern.singleton.demo;

/**
 * Created By 86188
 * on 2020/7/25
 * 单例模式--懒汉式：
 * 在第一次调用时，才初始化对象，并在内存中分配内存地址，之后的调用都是指向这个地址
 * 优点：可以在类需要调用时才初始化
 * 缺点：多线程情况下会出现多次初始化
 */
public class SingletonLazy {
    public static volatile SingletonLazy INSTANCE;//加volatile防止JIT语序重排时，没有初始化就返回INSTANCE

    private SingletonLazy() {

    }

    /* version_1:线程不安全 */
    public static SingletonLazy getInstanceV1() {
        return INSTANCE == null ? (INSTANCE = new SingletonLazy()) : INSTANCE;
    }

    /* version_2:方法加锁，可以解决线程不安全的问题，但是因为方法是static，所以效率大大降低 */
    /* synchronized是对类的当前实例进行加锁，防止其他线程同时访问该类的该实例的所有synchronized块。 */
    /* static synchronized是限制线程同时访问jvm中该类的所有实例同时访问对应的代码块。且一个类的所有静态方法公用一把锁。 */
    public static synchronized SingletonLazy getInstanceV2() {
        return INSTANCE == null ? (INSTANCE = new SingletonLazy()) : INSTANCE;
    }

    /* version_3：方法内加锁_方式1：不能解决线程安全，因为假设AB线程同时进入if，A得到锁，B等待，A执行完初始化了，此时B线程得到锁，再次初始化了 */
    public static SingletonLazy getInstanceV3() {
        if (INSTANCE == null) {
            synchronized (SingletonLazy.class) {
                INSTANCE = new SingletonLazy();
            }
        }
        return INSTANCE;
    }

    /* version_4：方法内加锁_方式2：(双重检测)可以解决线程问题，也可以加快效率，相较于上个版本，B得到锁后因INSTANCE不是null，所以不进下面的if，直接返回INSTANCE */
    public static SingletonLazy getInstanceV4() {
        if (INSTANCE == null) {//这步判断必须要，可以减少大多数线程获取锁
            synchronized (SingletonLazy.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SingletonLazy();
                }
            }
        }
        return INSTANCE;
    }
}
