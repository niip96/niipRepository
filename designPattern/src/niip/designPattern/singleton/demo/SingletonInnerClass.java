package niip.designPattern.singleton.demo;

/**
 * Created By 86188
 * on 2020/7/25
 * 单例模式--静态内部类方式
 * 类加载到内存时，不会去加载内部类，所以通过静态内部类去调用类的构造器初始化对象，既可以做到按需初始化，也可以保证线程安全
 * 如何保证线程安全？JVM加载类只会加载一次，内部类也只会加载一次，所以永远只分配一次内存地址
 */
public class SingletonInnerClass {

    private SingletonInnerClass() {
    }

    private static class MyHolder {
        private final static SingletonInnerClass INSTANCE = new SingletonInnerClass();
    }

    public static SingletonInnerClass getInstance() {
        return MyHolder.INSTANCE;
    }
}
