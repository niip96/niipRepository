package niip.designPattern.singleton.demo;

/**
 * Created By 86188
 * on 2020/7/25
 * 单例模式--枚举方式
 * 不仅解决了线程安全，还可以防止反序列化
 */
public enum SingletonEnum {

    INSTANCE;
}
