package niip.designPattern.singleton.demo;

import java.lang.reflect.Constructor;

/**
 * Created By 86188
 * on 2020/7/25
 * 单例模式--饿汉式:
 * 类加载到内存时就初始化一个对象，并在内存中分配内存地址，以后调用获取到的对象都是指向同一个内存地址
 * 优点：线程安全(加final后可以防止反射修改对象指向的内存地址)
 * 缺点：类一旦加载就要在分配内存地址给初始化的对象，可能会造成内存长时间被无效占用
 */
public class SingletonHungry {

    //加final保证其指向的内存引用地址不会改变，一般情况不加final地址也不会改变，但是通过反射构建的实例内存地址会不一致
    private final static SingletonHungry INSTANCE = new SingletonHungry();

    private SingletonHungry() {
    }

    public static SingletonHungry getInstance() {
        return INSTANCE;
    }
}
