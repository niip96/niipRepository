package niip.designPattern.singleton;

import niip.designPattern.singleton.demo.SingletonEnum;
import niip.designPattern.singleton.demo.SingletonHungry;
import niip.designPattern.singleton.demo.SingletonInnerClass;
import niip.designPattern.singleton.demo.SingletonLazy;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/**
 * Created By 86188
 * on 2020/7/25
 * 单例模式
 */
public class TestSingleton {

    public static void main(String[] args) {
        // testHungry();//推荐
        // testHungryFinal();
        // testLazyV1();
        // testLazyV2();
        // testLazyV3();
        // testLazyV4();
        // testInnerClass();//推荐
        // testEnum();
    }

    /* 饿汉式 */
    public static void testHungry() {
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                System.out.println(SingletonHungry.getInstance().hashCode());
            }).start();
        }
    }

    /* 懒汉式_v1 */
    public static void testLazyV1() {
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                System.out.println(SingletonLazy.getInstanceV1().hashCode());
            }).start();
        }
    }

    /* 懒汉式_v2 */
    public static void testLazyV2() {
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                System.out.println(SingletonLazy.getInstanceV2().hashCode());
            }).start();
        }
    }

    /* 懒汉式_v3 */
    public static void testLazyV3() {
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                System.out.println(SingletonLazy.getInstanceV3().hashCode());
            }).start();
        }
    }

    /* 懒汉式_v4 */
    public static void testLazyV4() {
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                System.out.println(SingletonLazy.getInstanceV4().hashCode());
            }).start();
        }
    }

    /* 静态内部类式 */
    public static void testInnerClass() {
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                System.out.println(SingletonInnerClass.getInstance().hashCode());
            }).start();
        }
    }

    /* 枚举类式 */
    public static void testEnum() {
        for (int i = 0; i < 50; i++) {
            new Thread(() -> {
                System.out.println(SingletonEnum.INSTANCE.hashCode());
            }).start();
        }
    }

    /* 通过反射来验证final修饰的作用 */
    public static void testHungryFinal() {
        System.out.println("1=========" + SingletonHungry.getInstance().hashCode());
        try {
            Class<SingletonHungry> clazz = SingletonHungry.class;
            Constructor<SingletonHungry> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            SingletonHungry sh = constructor.newInstance();
            System.out.println("2=========" + sh.hashCode());
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                System.out.println("3=========" + field.get(SingletonHungry.getInstance()).hashCode());
                //把反射创建出来的对象赋值给单例对象
                //如果用final修饰了则会抛出异常：Can not set static final SingletonHungry field INSTANCE to SingletonHungry
                field.set(SingletonHungry.getInstance(), sh);
                System.out.println("4=========" + SingletonHungry.getInstance().hashCode());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("5=========" + SingletonHungry.getInstance().hashCode());
    }
}
